'use strict';

module.exports.create = function (bigconf, deps, app) {
  var Jwks = require('./jwks').create(app);
  var Grants = require('./grants').create(app);
  var Accounts = require('./accounts').create(app);

  // This tablename is based on the tablename found in the objects in model.js.
  // Instead of the snake_case the name with be UpperCammelCase, converted by masterquest-sqlite3.
  function attachSiteModels(req, res, next) {
    return req.getSiteStore().then(function (store) {
      req.Models = store;
      next();
    });
  }
  function attachSiteStore(tablename, req, res, next) {
    return req.getSiteStore().then(function (store) {
      req.Store = store[tablename];
      next();
    });
  }
  function detachSiteStore(req, res, next) {
    delete req.Store;
    next();
  }
  function authorizeIssuer(req, res, next) {
    var promise = require('./common').checkIssuerToken(req, req.params.sub).then(function () {
      next();
    });

    app.handleRejection(req, res, promise, '[issuer@oauth3.org] authorize req as issuer');
  }

  app.get(   '/jwks/:sub/:kid.json',            Jwks.restful.get);
  app.get(   '/jwks/:sub/:kid',                 Jwks.restful.get);
  // Everything but getting keys is only for the issuer
  app.use(   '/jwks/:sub',                      authorizeIssuer, attachSiteStore.bind(null, 'IssuerOauth3OrgJwks'));
  app.post(  '/jwks/:sub',                      Jwks.restful.saveNew);

  // Everything regarding grants is only for the issuer
  app.use(   '/grants/:sub',                    authorizeIssuer, attachSiteStore.bind(null, 'IssuerOauth3OrgGrants'));
  app.get(   '/grants/:sub',                    Grants.restful.getAll);
  app.get(   '/grants/:sub/:azp',               Grants.restful.getOne);
  app.post(  '/grants/:sub/:azp',               Grants.restful.saveNew);

  app.use(   '/access_token',                   attachSiteModels);
  app.post(  '/access_token/send_otp',          Accounts.restful.sendOtp);
  app.post(  '/access_token/:sub/:aud/:azp',    Accounts.restful.createToken);
  app.post(  '/access_token',                   Accounts.restful.createToken);

  app.use(   '/acl/profile',                    attachSiteModels);
  app.get(   '/acl/profile',                    Accounts.restful.getProfile);
  app.post(  '/acl/profile',                    Accounts.restful.setProfile);

  app.use(   '/acl/contact_nodes',              attachSiteModels);
  app.post(  '/acl/contact_nodes',              Accounts.restful.claimContact);
  app.post(  '/acl/contact_nodes/:id',          Accounts.restful.verifyContact);

  app.use(detachSiteStore);
};
