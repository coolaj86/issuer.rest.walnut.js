'use strict';

module.exports.create = function (bigconf, deps, app) {

  app.post(  '/session', function (req, res, next) {
    console.log('[issuer@oauth3.org] POST /session');
    next();
  });

  return app;
};
