'use strict';

var crypto = require('crypto');
var PromiseA = require('bluebird');
var OpErr = PromiseA.OperationalError;
var makeB64UrlSafe = require('./common').makeB64UrlSafe;

function thumbprint(jwk) {
  // To produce a thumbprint we need to create a JSON string with only the required keys for
  // the key type, with the keys sorted lexicographically and no white space. We then need
  // run it through a SHA-256 and encode the result in url safe base64.
  // https://stackoverflow.com/questions/42588786/how-to-fingerprint-a-jwk
  var keys;
  if (jwk.kty === 'EC') {
    keys = ['crv', 'x', 'y'];
  } else if (jwk.kty === 'RSA') {
    keys = ['e', 'n'];
  } else {
    return PromiseA.reject(new Error('invalid JWK key type ' + jwk.kty));
  }
  keys.push('kty');
  keys.sort();

  var missing = keys.filter(function (name) { return !jwk.hasOwnProperty(name); });
  if (missing.length > 0) {
    return PromiseA.reject(new Error('JWK of type '+jwk.kty+' missing fields ' + missing));
  }

  // I'm not 100% sure this behavior is guaranteed by a real standard, but when we use an array
  // as the replacer argument the keys are always in the order they appeared in the array.
  var jwkStr = JSON.stringify(jwk, keys);
  var hash = crypto.createHash('sha256').update(jwkStr).digest('base64');
  return PromiseA.resolve(makeB64UrlSafe(hash));
}

function sanitizeJwk(jwk) {
  // We need to sanitize the key to make sure we don't deliver any private keys fields if
  // we were given a key we could use to sign tokens on behalf of the user. We also don't
  // want to deliver the sub or any other PPIDs.
  var whitelist = ['kty', 'alg', 'kid', 'use'];
  if (jwk.kty === 'EC') {
    whitelist = whitelist.concat(['crv', 'x', 'y']);
  } else if (jwk.kty === 'RSA') {
    whitelist = whitelist.concat(['e', 'n']);
  }

  var result = {};
  whitelist.forEach(function (key) {
    result[key] = jwk[key];
  });
  return result;
}

function create(app) {
  var restful = {};

  async function getRawKey(req) {
    var store = await req.getSiteStore();

    if (req.params.kid === req.experienceId) {
      return store.IssuerOauth3OrgPrivateKeys.get(req.experienceId);
    }

    // The keys are stored by the issuer PPID, but the sub we have might be a different PPID
    // for a 3rd party.
    var issuerSub;
    try {
      issuerSub = await require('./common').getPrimarySub(store.IssuerOauth3OrgGrants, req.params.sub);
    } catch (err) {
      if (/collision/.test(err.message)) {
        err.message = 'PPID collision - unable to safely retrieve keys';
      }
      throw err;
    }

    if (!issuerSub) {
      throw new OpErr("unknown PPID '" + req.params.sub + "'");
    }
    return store.IssuerOauth3OrgJwks.get(issuerSub + '/' + req.params.kid);
  }

  restful.get = function (req, res) {
    var promise = PromiseA.resolve(getRawKey(req)).then(function (jwk) {
      if (!jwk) {
        throw new OpErr("no keys stored with kid '"+req.params.kid+"' for PPID "+req.params.sub);
      }
      return sanitizeJwk(jwk);
    });

    app.handlePromise(req, res, promise, "[issuer@oauth3.org] retrieve JWK");
  };
  restful.saveNew = function (req, res) {
    var jwk = req.body;
    var promise = thumbprint(jwk).then(function (kid) {
      if (jwk.kid && jwk.kid !== kid) {
        throw new OpErr('provided kid "'+jwk.kid+'" does not match calculated "'+kid+'"');
      }
      jwk.kid = kid;
      jwk.sub = req.params.sub;

      return req.Store.upsert(jwk.sub+'/'+jwk.kid, jwk);
    }).then(function () {
      return { success: true };
    });

    app.handlePromise(req, res, promise, "[issuer@oauth3.org] save JWK");
  };

  return {
    thumbprint: thumbprint,
    restful: restful,
  };
}

module.exports.thumbprint = thumbprint;
module.exports.create = create;
