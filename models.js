'use strict';

var apiname = 'issuer_oauth3_org';
var baseFields = [ 'createdAt', 'updatedAt', 'deletedAt' ];

module.exports = [
  {
    tablename: apiname + '_private_keys',
    idname: 'id',
    indices: baseFields.concat([ 'kty', 'kid' ]),
  },
  {
    tablename: apiname + '_codes',
    idname: 'id',
    indices: baseFields.concat([ 'code', 'expires' ]),
  },
  {
    tablename: apiname + '_accounts',
    idname: 'username',
    indices: baseFields.concat([ 'accountId' ]),
  },
  {
    tablename: apiname + '_contact_nodes',
    idname: 'id',
    indices: baseFields.concat([ 'accountId', 'verifiedAt', 'lastVerifiedAt' ]),
  },
  {
    tablename: apiname + '_jwks',
    idname: 'id',
    indices: baseFields.concat([ 'kty', 'kid', 'sub' ]),
  },
  {
    tablename: apiname + '_grants',
    idname: 'id',
    indices: baseFields.concat([ 'sub', 'azp', 'azpSub', 'scope' ]),
  },
];
